let remainingTicket = 0;
let ticketPrices = {
  oneWay: 5000,
  roundTrip: 10000,
  halfYear: 50000,
  fullYear: 100000,
};

function getElementValue(id) { //gets the element by ID value in HTML
  return document.getElementById(id).value;
}

function getElement(id) { //gets the element by ID in HTML
  return document.getElementById(id);
}

function computeTotal() { // Compute Discounted Total And Fill HTML Discounts
  let oneWayAmount = getElementValue('one-way-amount');
  let roundTripAmount = getElementValue('round-trip-amount');
  let halfYearAmount = getElementValue('half-year-amount');
  let fullYearAmount = getElementValue('full-year-amount');
  let promoCode = getElementValue('promo-code').toUpperCase();

  let totalWithDiscount = getElement('total-with-discount');
  let discountOneWay = getElement('discount-one-way');
  let discountRoundTrip = getElement('discount-round-trip');
  let discountHalfYear = getElement('discount-half-year');
  let discountFullYear = getElement('discount-full-year');
  let discountPromoCode = getElement('discount-promo-code');
  let discountTotal = getElement('discount-total-discount');
  let totalNoDiscount = getElement("total-no-discount");
  let total = 0;

  if (oneWayAmount < 0 || roundTripAmount < 0 || halfYearAmount < 0 || fullYearAmount < 0) {
    discountTotal.textContent = 'You have a negative quantity in the order. Please correct it.'
    return;
  } else {
    discountTotal.textContent = '';
  }
      
  if (oneWayAmount > 0) {
    let freeOneWay = ticketPrices.oneWay*Math.floor(oneWayAmount/3);
    total += (oneWayAmount*ticketPrices.oneWay) - freeOneWay;
    if (freeOneWay > 0) {
      discountOneWay.textContent = 'Discount: ₱' + freeOneWay.toLocaleString('en', { useGrouping: true });
    }
  }
  
  if (roundTripAmount <= 5 && roundTripAmount > 0) {
    total += roundTripAmount*ticketPrices.roundTrip*0.7;
    discountRoundTrip.textContent = 'Discount: ₱' + (roundTripAmount*ticketPrices.roundTrip*0.3).toLocaleString('en', { useGrouping: true });
  } else if (roundTripAmount > 5) {
    total += (ticketPrices.roundTrip*5*0.7) + ((roundTripAmount-5)*ticketPrices.roundTrip);
    discountRoundTrip.textContent = 'Discount: ₱35,000';
  }

  if (halfYearAmount >= 2) {
    remainingTicket = halfYearAmount%2;
    total += ((halfYearAmount-remainingTicket)*ticketPrices.halfYear*0.9) + (ticketPrices.halfYear*remainingTicket);
    discountHalfYear.textContent = 'Discount: ₱' + ((halfYearAmount-remainingTicket)*ticketPrices.halfYear*0.1).toLocaleString('en', { useGrouping: true });
  } else {
    total += halfYearAmount*ticketPrices.halfYear;
  }

  if (fullYearAmount > 0) {
    total += fullYearAmount*ticketPrices.fullYear*0.9;
    discountFullYear.textContent = 'Discount: ₱' + (fullYearAmount*ticketPrices.fullYear*0.1).toLocaleString('en', { useGrouping: true });
  }

  if (promoCode == '5OFF' && total !== 0) {
    discountPromoCode.textContent = 'Discount: ₱' + (total*0.05).toLocaleString('en', { useGrouping: true });
    total *= 0.95;
  } else if (promoCode == 'FREETWOWAY' && total !== 0) {
    discountPromoCode.textContent = 'You get a free extra round-trip ticket!';
  }

  if (total > 0) {
    totalWithDiscount.value = '₱' + total.toLocaleString('en', {useGrouping:true});
    let currentAmount = parseFloat(totalNoDiscount.value.replace('₱', '').replace(',', '')) || 0;
    discountTotal.textContent = 'Total Discount: ₱' + (currentAmount - total).toLocaleString('en', {useGrouping:true}); 
    changeButton();  
  }

  
}

function addTotal(amount) { //Adds the total without discounts
  let totalNoDiscount = getElement("total-no-discount");
  let currentAmount = parseFloat(totalNoDiscount.value.replace('₱', '').replace(',', '')) || 0;
  let addedAmount = currentAmount + amount;
  totalNoDiscount.value = '₱' + addedAmount.toLocaleString('en', { useGrouping: true });
}

function subtractTotal(amount) { //Subtracts the total without discounts
  let totalNoDiscount = getElement("total-no-discount");
  let currentAmount = parseFloat(totalNoDiscount.value.replace('₱', '').replace(',', '')) || 0;
  let subtractedAmount = currentAmount - amount;
  totalNoDiscount.value = '₱' + subtractedAmount.toLocaleString('en', { useGrouping: true });
}

function updateQuantity(inputId, operation) { //Updates the order quantity of tickets
  let input = getElement(inputId);
  let currentQuantity = Number(input.value);
  input.value = operation === 'increment' ? currentQuantity + 1 : currentQuantity - 1;
}

function incrementOneWay() {
  updateQuantity('one-way-amount', 'increment');
  addTotal(ticketPrices.oneWay);
}

function decrementOneWay() {
  updateQuantity('one-way-amount', 'decrement');
  subtractTotal(ticketPrices.oneWay);
}

function incrementRoundTrip() {
  updateQuantity('round-trip-amount', 'increment');
  addTotal(ticketPrices.roundTrip);
}

function decrementRoundTrip() {
  updateQuantity('round-trip-amount', 'decrement');
  subtractTotal(ticketPrices.roundTrip);
}

function incrementHalfYear() {
  updateQuantity('half-year-amount', 'increment');
  addTotal(ticketPrices.halfYear);
}

function decrementHalfYear() {
  updateQuantity('half-year-amount', 'decrement');
  subtractTotal(ticketPrices.halfYear);
}

function incrementFullYear() {
  updateQuantity('full-year-amount', 'increment');
  addTotal(ticketPrices.fullYear);
}

function decrementFullYear() {
  updateQuantity('full-year-amount', 'decrement');
  subtractTotal(ticketPrices.fullYear);
}

function changeButton() { //Changes button value after pressing
  let buttonElem = document.getElementById("checkout-button");
  if (buttonElem.value == "Checkout") {
    buttonElem.value = "Another order?";
  } else {
    location.reload(); //Restarts the program
  }
}